import {City} from "./city";

export class PersonalInfo {
    id: number;
    address: string;
    phoneNumber: string;
    additionalContacts: string;
    city: City;
    firstName: string;
    lastName: string;
}
