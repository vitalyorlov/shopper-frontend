import {Currency} from "./currency";

export class Item {
  id: number;
  name: string;
  description: string;
  price: string;
  currency: Currency;
  likesCount: string;
  isFavourite: boolean;
}
