import {Media} from "./media";
import {PersonalInfo} from "./personal-info";

export class User {
    id: number;
    login: string;
    email: string;
    password?: string;
    media?: Media;
    personalInfo?: PersonalInfo;
}
