
export class Item {
    id: number;
    name: string;
    description: string;
    price: string;
    likesCount: string;
    isFavourite: boolean;
}
