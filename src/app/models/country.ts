export class Country {
    id: number;
    abbr: string;
    name: string;
}
