import {Currency} from "./currency";
export class CurrencyRate {
    id: number;
    rate: string;
    sourceCurrency: Currency;
    targetCurrency: Currency;
}
