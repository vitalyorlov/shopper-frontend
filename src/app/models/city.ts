import {Country} from "./country";

export class City {
    id: number;
    name: string;
    postalCode: string;
    country: Country;
}
