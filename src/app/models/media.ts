export class Media {
    id: number;
    filename: string;
    url: string;
}
