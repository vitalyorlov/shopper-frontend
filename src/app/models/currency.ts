export class Currency {
    id: number;
    abbr: string;
    name: string;
}