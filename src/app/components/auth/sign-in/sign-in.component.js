/**
 * Created by d1l on 3/26/17.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var auth_service_1 = require("../../../services/auth.service");
var local_storage_service_1 = require("../../../services/local-storage.service");
var storage_params_1 = require("../../../constants/storage-params");
var SignInComponent = (function () {
    function SignInComponent(router, localStorageService, authService) {
        this.router = router;
        this.localStorageService = localStorageService;
        this.authService = authService;
    }
    SignInComponent.prototype.ngOnInit = function () {
        return;
    };
    SignInComponent.prototype.signIn = function () {
        var _this = this;
        console.log(this.login, this.password);
        this.loaderActive = true;
        this.authService.login(this.login, this.password)
            .then(function (data) {
            _this.localStorageService.saveData(storage_params_1.StorageParams.ACCESS_TOKEN, data.accessToken);
            _this.localStorageService.saveData(storage_params_1.StorageParams.USERNAME, data.username);
            _this.localStorageService.saveData(storage_params_1.StorageParams.USER_PHOTO, data.userPhoto);
            _this.loaderActive = false;
            _this.goToDashboard();
        });
    };
    SignInComponent.prototype.goToDashboard = function () {
        this.router.navigate(['dashboard/my-items']);
    };
    return SignInComponent;
}());
SignInComponent = __decorate([
    core_1.Component({
        selector: 'sg-sign-in',
        templateUrl: './sign-in.component.html',
        styleUrls: ['./sign-in-styles.css']
    }),
    __metadata("design:paramtypes", [router_1.Router,
        local_storage_service_1.LocalStorageService,
        auth_service_1.AuthService])
], SignInComponent);
exports.SignInComponent = SignInComponent;
//# sourceMappingURL=sign-in.component.js.map