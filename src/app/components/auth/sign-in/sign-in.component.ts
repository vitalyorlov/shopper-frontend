/**
 * Created by d1l on 3/26/17.
 */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AuthService} from "../../../services/auth.service";
import {LocalStorageService} from "../../../services/local-storage.service";
import {StorageParams} from "../../../constants/storage-params";

@Component({
    selector: 'sg-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in-styles.css']
})
export class SignInComponent implements OnInit {

    private login: string;
    private password: string;
    private loaderActive: boolean;

    constructor(
        private router: Router,
        private localStorageService: LocalStorageService,
        private authService: AuthService) {
    }

    ngOnInit(): void {
        return;
    }

    signIn(): void {
        console.log(this.login, this.password);
        this.loaderActive = true;
        this.authService.login(this.login, this.password)
            .then((data) => {
                this.localStorageService.saveData(StorageParams.ACCESS_TOKEN, data.accessToken);
                this.localStorageService.saveData(StorageParams.USERNAME, data.username);
                this.localStorageService.saveData(StorageParams.USER_PHOTO, data.userPhoto);
                this.loaderActive = false;
                this.goToDashboard();
            });
    }

    goToDashboard(): void {
        this.router.navigate(['dashboard/my-items']);
    }
}
