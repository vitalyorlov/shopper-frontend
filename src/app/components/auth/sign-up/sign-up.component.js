/**
 * Created by d1l on 3/26/17.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var location_service_1 = require("../../../services/location.service");
var city_1 = require("../../../models/city");
var user_1 = require("../../../models/user");
var auth_service_1 = require("../../../services/auth.service");
var local_storage_service_1 = require("../../../services/local-storage.service");
var storage_params_1 = require("../../../constants/storage-params");
var personal_info_1 = require("../../../models/personal-info");
var media_1 = require("../../../models/media");
var SignUpComponent = (function () {
    function SignUpComponent(router, locationService, localStorageService, authService) {
        this.router = router;
        this.locationService = locationService;
        this.localStorageService = localStorageService;
        this.authService = authService;
        this.countryInputVisibility = false;
    }
    SignUpComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.init();
        this.locationService.getCountries().then(function (data) {
            var countries = data;
            _this.activeCountry = countries[0];
            _this.countries = countries;
            _this.getCities();
        });
    };
    SignUpComponent.prototype.getCities = function () {
        var _this = this;
        this.locationService.getCitiesByCountryId(this.activeCountry.id.toString())
            .then(function (data) {
            _this.cities = data;
        });
    };
    SignUpComponent.prototype.changeCountriesMenuVisibility = function () {
        this.countryInputVisibility = !this.countryInputVisibility;
    };
    SignUpComponent.prototype.signUp = function () {
        var _this = this;
        console.log(this.user);
        this.loaderActive = true;
        this.authService.register(this.user)
            .then(function (data) {
            _this.localStorageService.saveData(storage_params_1.StorageParams.ACCESS_TOKEN, data.accessToken);
            _this.localStorageService.saveData(storage_params_1.StorageParams.USERNAME, data.username);
            _this.localStorageService.saveData(storage_params_1.StorageParams.USER_PHOTO, data.userPhoto);
            _this.loaderActive = false;
            _this.goToDashboard();
        });
    };
    SignUpComponent.prototype.goToDashboard = function () {
        this.router.navigate(['dashboard/my-items']);
    };
    SignUpComponent.prototype.init = function () {
        this.user = new user_1.User();
        this.user.personalInfo = new personal_info_1.PersonalInfo();
        this.user.personalInfo.city = new city_1.City();
        this.user.media = new media_1.Media();
    };
    return SignUpComponent;
}());
SignUpComponent = __decorate([
    core_1.Component({
        selector: 'sg-sign-up',
        templateUrl: './sign-up.component.html',
        styleUrls: ['./sign-up-styles.css']
    }),
    __metadata("design:paramtypes", [router_1.Router,
        location_service_1.LocationService,
        local_storage_service_1.LocalStorageService,
        auth_service_1.AuthService])
], SignUpComponent);
exports.SignUpComponent = SignUpComponent;
//# sourceMappingURL=sign-up.component.js.map