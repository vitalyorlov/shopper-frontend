/**
 * Created by d1l on 3/26/17.
 */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {LocationService} from "../../../services/location.service";
import {Country} from "../../../models/country";
import {City} from "../../../models/city";
import {User} from "../../../models/user";
import {AuthService} from "../../../services/auth.service";
import {LocalStorageService} from "../../../services/local-storage.service";
import {StorageParams} from "../../../constants/storage-params";
import {PersonalInfo} from "../../../models/personal-info";
import {Media} from "../../../models/media";

@Component({
    selector: 'sg-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up-styles.css']
})
export class SignUpComponent implements OnInit {

    private countries: Country[];
    private cities: City[];
    private activeCountry: Country;
    private loaderActive: boolean;
    user: User;
    password: string;
    countryInputVisibility: boolean = false;

    constructor(
        private router: Router,
        private locationService: LocationService,
        private localStorageService: LocalStorageService,
        private authService: AuthService) {
    }

    ngOnInit(): void {
        this.init();

        this.locationService.getCountries().then((data) => {
            let countries = data as Country[];
            this.activeCountry = countries[0];
            this.countries = countries;
            this.getCities();
        });
    }

    private getCities(): void {
        this.locationService.getCitiesByCountryId(this.activeCountry.id.toString())
            .then((data) => {
                this.cities = data as City[];
            });
    }

    changeCountriesMenuVisibility() {
        this.countryInputVisibility = !this.countryInputVisibility;
    }

    signUp(): void {
        console.log(this.user);
        this.loaderActive = true;
        this.authService.register(this.user)
            .then((data) => {
                this.localStorageService.saveData(StorageParams.ACCESS_TOKEN, data.accessToken);
                this.localStorageService.saveData(StorageParams.USERNAME, data.username);
                this.localStorageService.saveData(StorageParams.USER_PHOTO, data.userPhoto);
                this.loaderActive = false;
                this.goToDashboard();
            });
    }

    goToDashboard(): void {
        this.router.navigate(['dashboard/my-items']);
    }

    private init() {
        this.user = new User();
        this.user.personalInfo = new PersonalInfo();
        this.user.personalInfo.city = new City();
        this.user.media = new Media();
    }
}
