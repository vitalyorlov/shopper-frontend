import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { DashboardRoutingModule } from './dashboard-routing.module';
import {AppointmentsComponent} from "./appointments/appointments.component";
import {BookmarksComponent} from "./bookmarks/bookmarks.component";
import {MyItemsComponent} from "./my-items/my-items.component";
import {RequestsComponent} from "./requests/requests.component";
import {DashboardComponent} from "./dashboard.component";
import {SidebarComponent} from "../shared/sidebar/sidebar.component";
import {DashboardHeaderComponent} from "../shared/dashboard-header/dashboard-header.component";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        DashboardRoutingModule
    ],
    declarations: [
        DashboardComponent,
        AppointmentsComponent,
        BookmarksComponent,
        MyItemsComponent,
        RequestsComponent,
        SidebarComponent,
        DashboardHeaderComponent
    ],
    providers: [],
    bootstrap: [ DashboardComponent ]
})
export class DashboardModule { }/**
 * Created by d1l on 4/8/17.
 */
