/**
 * Created by d1l on 3/26/17.
 */

import {AfterViewInit, Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'sg-bookmarks-page',
    templateUrl: './bookmarks.component.html',
    styleUrls: ['./../dashboard-styles.css']
})
export class BookmarksComponent implements OnInit {

    constructor(
        private router: Router) {
    }

    ngOnInit(): void {
        return;
    }

}
