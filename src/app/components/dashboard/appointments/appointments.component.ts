/**
 * Created by d1l on 3/26/17.
 */

import {AfterViewInit, Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'sg-appointments-page',
    templateUrl: './appointments.component.html',
    styleUrls: ['./../dashboard-styles.css']
})
export class AppointmentsComponent implements OnInit {

    constructor(
        private router: Router) {
    }

    ngOnInit(): void {
        return;
    }

}
