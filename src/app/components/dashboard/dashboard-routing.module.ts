import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {AppointmentsComponent} from "./appointments/appointments.component";
import {BookmarksComponent} from "./bookmarks/bookmarks.component";
import {MyItemsComponent} from "./my-items/my-items.component";
import {RequestsComponent} from "./requests/requests.component";

const routes: Routes = [
    { path: 'dashboard', redirectTo: 'dashboard/my-items', pathMatch: 'full' },
    { path: 'dashboard/appointments',  component: AppointmentsComponent },
    { path: 'dashboard/bookmarks', component: BookmarksComponent },
    { path: 'dashboard/my-items',     component: MyItemsComponent },
    { path: 'dashboard/requests', component: RequestsComponent }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class DashboardRoutingModule {}
