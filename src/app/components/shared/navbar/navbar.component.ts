/**
 * Created by d1l on 4/2/17.
 */

import {Component, Input, OnInit} from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'sg-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar-styles.css']
})
export class NavbarComponent implements OnInit {

    @Input()
    activeItem: string;

    constructor(
        private router: Router) {
    }

    ngOnInit(): void {
        return;
    }

}