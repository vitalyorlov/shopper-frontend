/**
 * Created by d1l on 4/2/17.
 */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'sg-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer-styles.css']
})
export class FooterComponent implements OnInit {

    constructor(
        private router: Router) {
    }

    ngOnInit(): void {
        return;
    }

}