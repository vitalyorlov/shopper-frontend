/**
 * Created by d1l on 4/2/17.
 */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Item} from "../../../models/hero";

@Component({
    selector: 'sg-items-list',
    templateUrl: './items-list.component.html',
    styleUrls: ['./items-list-styles.css']
})
export class ItemsListComponent implements OnInit {

    items: Item[];

    constructor(
        private router: Router) {
    }

    ngOnInit(): void {
        this.items = [];
        for (let i = 0; i < 10 ; i++) {
            this.items.push(new Item());
        }
    }

    gotoDetail(item: Item): void {
        let link = ['/detail', item.id];
        this.router.navigate(link);
    }

}
