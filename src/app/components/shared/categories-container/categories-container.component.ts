/**
 * Created by d1l on 4/2/17.
 */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'sg-categories-container',
    templateUrl: './categories-container.component.html',
    styleUrls: ['./categories-container-styles.css']
})
export class CategoriesContainerComponent implements OnInit {

    constructor(
        private router: Router) {
    }

    ngOnInit(): void {
        return;
    }

}