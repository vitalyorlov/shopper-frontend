/**
 * Created by d1l on 4/2/17.
 */

import {Component, Input, OnInit} from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'sg-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar-styles.css']
})
export class SidebarComponent implements OnInit {

    constructor(
        private router: Router) {
    }

    ngOnInit(): void {
        return;
    }

}