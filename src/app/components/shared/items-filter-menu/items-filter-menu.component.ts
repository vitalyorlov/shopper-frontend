/**
 * Created by d1l on 4/2/17.
 */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Item} from "../../../models/hero";

@Component({
    selector: 'sg-items-filter-menu',
    templateUrl: './items-filter-menu.component.html',
    styleUrls: ['./items-filter-menu-styles.css']
})
export class ItemsFilterMenuComponent implements OnInit {

    constructor(
        private router: Router) {
    }

    ngOnInit(): void {
    }

}
