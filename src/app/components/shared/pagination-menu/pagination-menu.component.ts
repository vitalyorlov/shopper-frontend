/**
 * Created by d1l on 4/2/17.
 */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Item} from "../../../models/hero";

@Component({
    selector: 'sg-pagination-menu',
    templateUrl: './pagination-menu.component.html'
})
export class PaginationMenuComponent implements OnInit {

    constructor(
        private router: Router) {
    }

    ngOnInit(): void {
    }
}
