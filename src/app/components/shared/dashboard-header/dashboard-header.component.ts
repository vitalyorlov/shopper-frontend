/**
 * Created by d1l on 4/2/17.
 */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'sg-dashboard-header',
    templateUrl: './dashboard-header.component.html',
    styleUrls: ['./dashboard-header-styles.css']
})
export class DashboardHeaderComponent implements OnInit {

    constructor(
        private router: Router) {
    }

    ngOnInit(): void {
        return;
    }

}
