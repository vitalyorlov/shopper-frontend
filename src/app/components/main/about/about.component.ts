/**
 * Created by d1l on 3/26/17.
 */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'sg-about-page',
    templateUrl: './about.component.html',
    styleUrls: ['./about-styles.css']
})
export class AboutComponent implements OnInit {

    constructor(
        private router: Router) {
    }

    ngOnInit(): void {
        return;
    }

}

