/**
 * Created by d1l on 3/26/17.
 */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'sg-items-page',
    templateUrl: './items.component.html',
    styleUrls: ['./items-styles.css']
})
export class ItemsComponent implements OnInit {

    constructor(
        private router: Router) {
    }

    ngOnInit(): void {
        return;
    }

}
