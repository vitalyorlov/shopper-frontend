/**
 * Created by d1l on 3/26/17.
 */

import {AfterViewInit, Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'sg-home-page',
    templateUrl: './home.component.html',
    styleUrls: ['./home-styles.css']
})
export class HomeComponent implements OnInit, AfterViewInit {

    constructor(
        private router: Router) {
    }

    ngOnInit(): void {
        return;
    }

    ngAfterViewInit(): void {
        // fix menu when passed
        $('.masthead')
            .visibility({
                once: false,
                onBottomPassed: function() {
                    $('.fixed.menu').transition('fade in');
                },
                onBottomPassedReverse: function() {
                    $('.fixed.menu').transition('fade out');
                }
            })
        ;

        // create sidebar and attach to menu open
        $('.ui.sidebar')
            .sidebar('attach events', '.toc.item');
    }
}
