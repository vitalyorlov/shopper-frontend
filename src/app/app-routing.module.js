"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var home_component_1 = require("./components/main/home/home.component");
var help_component_1 = require("./components/main/help/help.component");
var about_component_1 = require("./components/main/about/about.component");
var sign_in_component_1 = require("./components/auth/sign-in/sign-in.component");
var sign_up_component_1 = require("./components/auth/sign-up/sign-up.component");
var items_component_1 = require("./components/main/items/items.component");
var dashboard_component_1 = require("./components/dashboard/dashboard.component");
var routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'help', component: help_component_1.HelpComponent },
    { path: 'about', component: about_component_1.AboutComponent },
    { path: 'home', component: home_component_1.HomeComponent },
    { path: 'advertisements', component: items_component_1.ItemsComponent },
    { path: 'sign-in', component: sign_in_component_1.SignInComponent },
    { path: 'sign-up', component: sign_up_component_1.SignUpComponent },
    { path: 'dashboard', component: dashboard_component_1.DashboardComponent }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.RouterModule.forRoot(routes)],
        exports: [router_1.RouterModule]
    })
], AppRoutingModule);
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map