export class StorageParams {
    public static ACCESS_TOKEN = "accessToken";
    public static USERNAME = "username";
    public static USER_PHOTO = "userPhoto";
}
