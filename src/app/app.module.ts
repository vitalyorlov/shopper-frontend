import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent }         from './app.component';
import { HeroService }          from './services/hero.service';
import { HomeComponent } from './components/main/home/home.component';
import { AboutComponent } from './components/main/about/about.component';
import { HelpComponent } from './components/main/help/help.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { SignUpComponent } from './components/auth/sign-up/sign-up.component';
import { SignInComponent } from './components/auth/sign-in/sign-in.component';
import { ItemsComponent } from './components/main/items/items.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import {CategoriesContainerComponent} from "./components/shared/categories-container/categories-container.component";
import {DashboardModule} from "./components/dashboard/dashboard.module";
import {ItemsContainerComponent} from "./components/shared/items-container/items-container.component";
import {ItemsListComponent} from "./components/shared/items-list/items-list.component";
import {ItemsFilterMenuComponent} from "./components/shared/items-filter-menu/items-filter-menu.component";
import {PaginationMenuComponent} from "./components/shared/pagination-menu/pagination-menu.component";
import {AuthService} from "./services/auth.service";
import {HttpService} from "./services/http.service";
import {LocalStorageService} from "./services/local-storage.service";
import {LocationService} from "./services/location.service";

@NgModule({
  imports: [
      BrowserModule,
      FormsModule,
      HttpModule,
      AppRoutingModule,
      DashboardModule
  ],
  declarations: [
      AppComponent,
      HelpComponent,
      AboutComponent,
      HomeComponent,
      NavbarComponent,
      FooterComponent,
      SignUpComponent,
      SignInComponent,
      ItemsComponent,
      CategoriesContainerComponent,
      ItemsContainerComponent,
      ItemsListComponent,
      ItemsFilterMenuComponent,
      PaginationMenuComponent
  ],
  providers: [ HeroService, AuthService, HttpService, LocalStorageService, LocationService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
