import { Injectable }    from '@angular/core';

import 'rxjs/add/operator/toPromise';
import {User} from "../models/user";
import {HttpService} from "./http.service";

@Injectable()
export class AuthService {

    private loginUrl = 'login';
    private signUpUrl = 'sign-up';

    constructor(private httpService: HttpService) { }

    login(login: string, password: string): Promise<any> {
        let user = new User();
        user.login = login;
        user.password = password;

        return this.httpService
            .post(this.loginUrl, user);
    }

    register(user: User): Promise<any> {
        return this.httpService
            .post(this.signUpUrl, user);
    }
}

