import { Injectable }    from '@angular/core';

import 'rxjs/add/operator/toPromise';
import {User} from "../models/user";
import {HttpService} from "./http.service";

@Injectable()
export class LocationService {

    private citiesUrl = 'cities';
    private countriesUrl = 'countries';
    private countryUrl = 'country';

    constructor(private httpService: HttpService) { }

    getCitiesByCountryId(countryId: string): Promise<any> {
        const url = `${this.countryUrl}/${countryId}/${this.citiesUrl}`;
        return this.httpService.get(url);
    }

    getCountries(): Promise<any> {
        const url = `${this.countriesUrl}`;
        return this.httpService.get(url);
    }
}
