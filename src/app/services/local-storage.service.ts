import { Injectable }    from '@angular/core';

@Injectable()
export class LocalStorageService {

    constructor() { }

    saveData(key: string, data: string) {
        localStorage.setItem(key, JSON.stringify(data));
    }

    getData(key: string): any {
        return JSON.parse(localStorage.getItem(key));
    }

}

