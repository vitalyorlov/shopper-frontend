import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import {LocalStorageService} from "./local-storage.service";
import {StorageParams} from "../constants/storage-params";

@Injectable()
export class HttpService {

  private headers = new Headers({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  });
  private baseUrl = 'http://localhost:8080/';  // URL to web api

  constructor(private http: Http,
  private localStorageService: LocalStorageService) { }

  get(url: string): Promise<any> {
      this.addAuthHeader();
      return this.http.get(this.baseUrl + url, {headers: this.headers})
               .toPromise()
               .then(response => response.json())
               .catch(this.handleError);
  }

  delete(url: string): Promise<void> {
      this.addAuthHeader();
      return this.http.delete(this.baseUrl + url, {headers: this.headers})
          .toPromise()
          .then(() => null)
          .catch(this.handleError);
  }

  post(url: string, data: any): Promise<any> {
      this.addAuthHeader();
      return this.http
          .post(this.baseUrl + url, JSON.stringify(data), {headers: this.headers})
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
  }

  update(url: string, data: any): Promise<any> {
      this.addAuthHeader();
      return this.http
          .put(this.baseUrl + url, JSON.stringify(data), {headers: this.headers})
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  private addAuthHeader() {
      this.headers.append('Authorization',
          this.localStorageService.getData(StorageParams.ACCESS_TOKEN));
  }
}

