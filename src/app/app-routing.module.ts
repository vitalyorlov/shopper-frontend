import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/main/home/home.component';
import { HelpComponent } from './components/main/help/help.component';
import { AboutComponent } from './components/main/about/about.component';
import { SignInComponent } from './components/auth/sign-in/sign-in.component';
import { SignUpComponent } from './components/auth/sign-up/sign-up.component';
import { ItemsComponent } from './components/main/items/items.component';
import {DashboardComponent} from "./components/dashboard/dashboard.component";

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'help',  component: HelpComponent },
  { path: 'about', component: AboutComponent },
  { path: 'home',     component: HomeComponent },
  { path: 'advertisements', component: ItemsComponent },
  { path: 'sign-in', component: SignInComponent },
  { path: 'sign-up',     component: SignUpComponent },
  { path: 'dashboard',     component: DashboardComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
