"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var hero_service_1 = require("./services/hero.service");
var home_component_1 = require("./components/main/home/home.component");
var about_component_1 = require("./components/main/about/about.component");
var help_component_1 = require("./components/main/help/help.component");
var navbar_component_1 = require("./components/shared/navbar/navbar.component");
var sign_up_component_1 = require("./components/auth/sign-up/sign-up.component");
var sign_in_component_1 = require("./components/auth/sign-in/sign-in.component");
var items_component_1 = require("./components/main/items/items.component");
var footer_component_1 = require("./components/shared/footer/footer.component");
var categories_container_component_1 = require("./components/shared/categories-container/categories-container.component");
var dashboard_module_1 = require("./components/dashboard/dashboard.module");
var items_container_component_1 = require("./components/shared/items-container/items-container.component");
var items_list_component_1 = require("./components/shared/items-list/items-list.component");
var items_filter_menu_component_1 = require("./components/shared/items-filter-menu/items-filter-menu.component");
var pagination_menu_component_1 = require("./components/shared/pagination-menu/pagination-menu.component");
var auth_service_1 = require("./services/auth.service");
var http_service_1 = require("./services/http.service");
var local_storage_service_1 = require("./services/local-storage.service");
var location_service_1 = require("./services/location.service");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            app_routing_module_1.AppRoutingModule,
            dashboard_module_1.DashboardModule
        ],
        declarations: [
            app_component_1.AppComponent,
            help_component_1.HelpComponent,
            about_component_1.AboutComponent,
            home_component_1.HomeComponent,
            navbar_component_1.NavbarComponent,
            footer_component_1.FooterComponent,
            sign_up_component_1.SignUpComponent,
            sign_in_component_1.SignInComponent,
            items_component_1.ItemsComponent,
            categories_container_component_1.CategoriesContainerComponent,
            items_container_component_1.ItemsContainerComponent,
            items_list_component_1.ItemsListComponent,
            items_filter_menu_component_1.ItemsFilterMenuComponent,
            pagination_menu_component_1.PaginationMenuComponent
        ],
        providers: [hero_service_1.HeroService, auth_service_1.AuthService, http_service_1.HttpService, local_storage_service_1.LocalStorageService, location_service_1.LocationService],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map